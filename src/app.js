const path = require('path');
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();


//Import routes
const indexRoutes = require('./routes/index');

//Conectando a la DB
mongoose.connect('mongodb://localhost/crud-node', {useNewUrlParser: true})
  .then(db => console.log('DB conectada'))
  .catch(err => console.log(error));

//settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//Middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));

//routes
app.use('/', indexRoutes);//le quité el punto a la raiz

app.listen(app.get('port'), () => {
  console.log(`listening on port ${app.get('port')}`);
});
